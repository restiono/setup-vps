#!/bin/bash

_WHERE=`pwd`
SETUP=$_WHERE"/setup-debian.sh"

# check for argumen
if [ $# -eq 0 ]
then
	echo "Maaf argumen harus ada, ketik x86 atau arm."
	exit 77
else
	if [ $1 == 'x86' ]
	then 
		echo "Adding dotdeb repository..."
		$SETUP dotdeb 
		sleep 3
	fi
	
	echo "Cleaning up the system..."
	$SETUP system 
	sleep 3
	
	echo "Setup IPTABLES Firewall..."
	$SETUP iptables 22 
	sleep 3
	
	echo "Setup nginx as web server..."
	$SETUP nginx 
	sleep 3
		
	echo "Setup mysql as database server..."
	$SETUP mysql 
	sleep 3
	if [ $2 == 'php7' ]
	then
		echo "Setup PHP 7.0 as intepreter..."
		$SETUP php7 
		sleep 3
	else
		echo "Setup PHP 5.6 as intepreter..."
		$SETUP php 
		sleep 3
	fi
	# Show the thank you message
	echo "Your VPS is ready. Thank you for using this script."
fi
