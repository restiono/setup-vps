#!/bin/bash

# check for argumen
if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 77
fi

# backup begin
DUMPER=`which mysqldump`
DATE=`date +%Y%m%d_%H%M%S`
FIND=`which find`
MOUNT=`which mount`

# change to 1 if you want to backup using cloud
_USECLOUD=1
# change it to your cloud mounted dir
_CLOUDDIR="/mnt/4shared"

_DIR="/var/www/$1"
_WPDIR="$_DIR/public"
_CONFIG="$_WPDIR/wp-config.php"
_BACKUPDIR="$_DIR/backup"

_DB=`cat $_CONFIG | grep DB_NAME | cut -d \' -f 4`
_USER=`cat $_CONFIG | grep DB_USER | cut -d \' -f 4`
_PASS=`cat $_CONFIG | grep DB_PASSWORD | cut -d \' -f 4`

function start_backup() {
	# Check backup dir and create if doesn't exists
	[ ! -d $_BACKUPDIR ] && mkdir $_BACKUPDIR
	_TMPDIR="$_BACKUPDIR/$_DB-$DATE"
	[ ! -d $_TMPDIR ] && mkdir $_TMPDIR
	_SQLFILE="$_TMPDIR/$_DB-$DATE.sql"
	$DUMPER -u $_USER -p$_PASS $_DB > $_SQLFILE

	cp -a $_WPDIR $_TMPDIR
	
	_DIRNOW=`pwd`
	cd $_BACKUPDIR
	_BASEDIR=`basename $_TMPDIR`
	tar -czf $_DB-$DATE.tgz $_BASEDIR
	
	# Check if cloud is used and directory is mounted
	if [ $_USECLOUD -eq 1 ]
	then
		$MOUNT | grep $_CLOUDDIR
		if [ $? -eq 0 ]
		then
			cp $_DB-$DATE.tgz $_CLOUDDIR
		else
			$MOUNT $_CLOUDDIR
			cp $_DB-$DATE.tgz $_CLOUDDIR
		fi
		$FIND $_CLOUDDIR -mtime +7 -exec rm {} \;
		sleep 3
		umount $_CLOUDDIR
	fi
	
	# Clean up after backup and everything
	rm -rf $_TMPDIR
	
	# back to original directory
	cd $_DIRNOW
	
	# Find and Remove all files within 7 days.
	$FIND $_BACKUPDIR -mtime +7 -exec rm {} \;
}

case "$1" in
	"")
	echo "Maaf, argumen pertama harus benar"
	exit 77
	;;
	"-f")
	if [ $2 -eq 0]
		then
			echo "Maaf, silahkan masukkan file config"
			exit 78
		else
			echo "Backup pakai file"
			exit 78
	fi
	;;
	*)
		start_backup
	;;
esac
